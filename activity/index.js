// console.log("Hello world");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function getUserProfile(){

        let fullName = prompt('Enter your Full Name: ');
        let age = prompt('Enter your age: ');
        let location = prompt('Enter your location: ');
    
        alert('Thank you, ' + fullName + ' for your info');
        console.log('Your full name is : ' + fullName);
        console.log('Your age is : ' + age);
        console.log('You are located at ' + location);
    
    }
    
    getUserProfile();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    function showMyFaveArtists(){
        let artists = ['Alanis Morisette','Sheryl Crow', 'Gwen Stefani','Imagine Dragons','Eraserheads'];
        console.log(artists);
    }

    showMyFaveArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function showMyFaveMovies(){
        let movies = [
            {movie: 'Titanic', rating: '87%'},
            {movie: 'Shawshank Redemption', rating: '91%'},
            {movie: 'Inception', rating: '87%'},
            {movie: 'Interstellar', rating: '73%'},
            {movie: 'Avengers: Endgame', rating: '94%'}
        ];

        console.log('My Top 5 Favorite Movies');
        console.log(movies);


    }

    showMyFaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);