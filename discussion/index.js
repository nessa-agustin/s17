// console.log('hello world');
/* 
    functions are lines/block of codes that tell our device/application to perform certain tasks when called/invoked
    -functions are mostly created to create complicated task to run a several lines in succession
    -they are also used to prevent repeating lines/blocks of code that perform the same task/function


    syntax:
        function functionName(){
            code block(statements)
        }

        >>function keyword use to define a javascript functions
        >>functionName - functions are named to be used later in the code
        >>function block - these are statements which comprise the body of function. this is where the code to be executed
*/

function printName(){
    console.log('My name is Jungkook');


};

//function invocation - it is common to use the term "call a function" instead of "invoke a function"

printName();

declaredFunction();
// result error, because it is not yet defined

function declaredFunction(){
    console.log('this is a defined function.');


}

/* Function Declaration vs Expression
        A function can be created thru function declaration by using function keyword and adding a function name

        Declared function are not executed immediately. They are "saved for later use" and will be executed later when they are invoked (called upon)



 */

function declaredFunction2(){
    console.log('Hi I am from declared function().');

}

declaredFunction2(); //declared functions can be hoisted as long as the function has been defined.


/* 
    function expression
    -a function can be stored in a variable. this is called a function expression

    -anonyous function - function without a name
*/

let variableFunction = function(){
    console.log('I am from variable function');
}

variableFunction();

/* we can also create a function expression of a named function. However, to invode the function expression we invoke it by its variable name, not by its function name
    Function expressions are always invoked using the variable name

*/

let funcExpression = function funcName(){
    console.log("hello from the other side");
}

funcExpression();


/* 
Function Scoping

Scope is the accessibility(visibility) of variables within our program
Javscript variables has 3 types of scope:
1. Local/block scope
2. global scope
3. function scope

*/

{
    let localVar = "Kim Seok Jin";
}

let globalVar = "The World's Most Handsome";

// console.log(localVar);
/* a local variable will be visible only within a function, where it is defined
 */
console.log(globalVar);

/* a global variable has global scope, which means it can be defined anywhere in your Js code
 */

/* Function scope
Javascript has a function scope: Each function creates a new scpoe.

Variables defined inside a function are not accessible from outisde function

Variables declared with var, let and const are quite similar when declatred inside a function

*/

function showNames(){
    var functionVar = 'Jungkook';
    const functionConst = 'BTS';
    let functionLet = 'kookie';

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}

showNames();

/* Nested Function
You can create another function inside a function. This is called nested function. This nested function, being inside a new function will have access to variable, name as they are within the same scope or code block

*/

function myNewFunction(){
    let name = 'Yor';

    function nestedFunction(){
        let nestedName = 'Brando';
        console.log(nestedName);
    }
}

// Function and Global Scoped Variables
// Global Scoped Variable

let globalName = 'Thonie';

function myNewFunction2(){
    let nameInside = 'Kim';
    /* Variables declared globally (outside function) have global scope
    Global variables can be accessed from anywhere in a javascript program including from inside a function
     */
    console.log(globalName);
}

myNewFunction2();

function showSampleAlert(){
    alert('Hello user!');
}

showSampleAlert();

console.log('I will only log in the console when the alert is dismissed');

/* 
    prompt()
    syntax: prompt('<dialog>')
*/

let samplePrompt = prompt('Enter your name:');
console.log('Hello ' + samplePrompt);


let sampleNullPrompt = prompt('Dont put anything');
console.log(sampleNullPrompt);


function printWelcomeMessage(){
    let firstName = prompt('Enter your first name: ');
    let lastName = prompt('Enter your last name: ');

    console.log('Hello, ' + firstName + lastName + '!');
    console.log("Welcome to Gamer's Guild");


};

printWelcomeMessage();

// Function Naming Convention
/* Function should be definitive of its task.
usually contains verb 



*/

function getCourses(){
    let course = ['Programming 100','SCience101','Grammar102','Math103'];

    console.log(course);
}

getCourses();

//avoid generic names to avoid confusion
// name function in smallcaps, follow camelCase when naming functions

function displayCarInfo(){

    console.log('Brand: Toyota');
    console.log('Type: Sedan');
    console.log('Price: 1,500,000');
}

displayCarInfo();